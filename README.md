Ansible scripts to handle OEV-GEODATA machines
==============================================

This repository contains various scripts to setup and configure machines
handled by OEV-GEODATA team (a team for 3D with OpenStreetMap). These
scripts are used with [ansible][http://www.ansibleworks.com/].

