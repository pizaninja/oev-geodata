#!/bin/sh

PYTHONUNBUFFERED=1 ANSIBLE_FORCE_COLOR=true \
ANSIBLE_HOST_KEY_CHECKING=false \
ANSIBLE_SSH_ARGS='-o UserKnownHostsFile=/dev/null -o ControlMaster=auto -o ControlPersist=60s' \
ansible-playbook \
--ask-pass \
--connection=ssh \
--inventory-file=inventoryInit \
-vvvv \
setupDebianOvh.yml
